# oOh Media exercise

A [Node.js](https://github.com/nodejs/node) project in [TypeScript](https://github.com/Microsoft/TypeScript)

- [TypeScript](https://github.com/Microsoft/TypeScript) for ES6 transpilation and Type definitions for Node.js v10 and Jest
- [TSLint](https://github.com/palantir/tslint), [.editorconfig](https://editorconfig.org/) and [Prettier](https://github.com/prettier/prettier)for consistent file formatting.
- [Jest](https://github.com/facebook/jest) unit testing and code coverage,
- [TypeORM](https://github.com/typeorm/typeorm) for Entity management
- [Postgres](https://github.com/postgres/postgres) to persist data
- [Redis](https://github.com/antirez/redis) to cache database requests
- [NPM scripts for common operations](#available-scripts),
- Examples of Typescript Unit Tests using [ts-jest](https://github.com/kulshekhar/ts-jest)

## Quick start

This project is intended to be used with v10 release of [Node.js](https://github.com/nodejs/node) or newer and [NPM](https://github.com/npm/cli). Make sure you have those installed.

A `.env` file is provided in thi repository, these can be configured as required, the defaults will work out of the box.

To start a dockerized environment with the application running type following commands:

```sh
npm install
./scripts/start.sh
```

### Authentication

In a real world scenario, authentication / security requires a large amount of forethought, effort and consideration. Rather than implement a half baked password solution, in this implementation a user can retrieve a login purely by providing their email address.

To begin sending requests to the api, you should first retrieve a JWT by running the below command.

```
curl -X POST \
  http://localhost:8080/v1/login \
  -H 'Authorization: Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwiaWF0IjoxNTMzMDUxNTMyfQ.XTsGIABgpXhKC79ZJVWwxY8GquFMW4cus-0pd4jYSpI' \
  -d '{
	  "email": "mitchell.rowling@gmail.com"
  }'
```

All other endpoints require the `Authorization` header to be set as shown in the example below.

```
curl -X POST \
  http://localhost:8080/v1/user\
  -H 'Authorization: Bearer jwtTokenGoesHere'
```

Any changes to the data via the API automatically creates/updates the following properties on the associated records:

- createdAt Timestamp of creation
- createdBy User Id of user that created
- updatedAt Timestamp of last update
- updatedBy User Id of user that last updated

--

### API Documentation

Api Documentation is located in the `openApi.yml` file. It can be viewed online using [Swagger Editor](https://editor.swagger.io/#)

### Database Seeding

The database is configured to be seeded by the `./db/seed.sql` file. This can be updated as necessary.

### Testing

Tests are located in the `__tests__` directory. To run full integration tests in a dockerized environment type the following commands:

```sh
npm install
./scripts/test.sh
```

This will also produce a code coverage report. Due to time limitations, Code Coverage is currently very low, however the core concepts are demonstrated and can be hooked into a CI/CD pipeline.

## Available scripts

- `clean` - remove coverage data, Jest cache and transpiled files,
- `build` - transpile TypeScript to ES6,
- `watch` - interactive watch mode to automatically transpile source files,
- `lint` - lint source files and tests,
- `test` - run tests,
- `test:watch` - interactive watch mode to automatically re-run tests
- `start` - starts the node application
