import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { ShoppingCentre } from './ShoppingCentre';
import { User } from './User';

@Entity()
export class Asset {
  // Auto Generated Columns

  @PrimaryGeneratedColumn() public id: number;

  @Column() public name: string;

  @Column() public location: string;

  @Column() public isActive: boolean;

  @Column() public width: number;

  @Column() public height: number;

  @Column() public createdBy: number;

  @Column() public updatedBy: number;

  @CreateDateColumn() public createdAt: string;

  @UpdateDateColumn() public updatedAt: string;

  // Relationships

  @ManyToOne(
    () => ShoppingCentre,
    (shoppingCentre: ShoppingCentre) => shoppingCentre.assets
  )
  public shoppingCentre: ShoppingCentre;

  @ManyToOne(() => User, (user: User) => user.updatedAssets)
  public updatedUser: User;

  @ManyToOne(() => User, (user: User) => user.createdAssets)
  public createdUser: User;
}
