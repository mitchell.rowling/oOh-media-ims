import {
  Column,
  CreateDateColumn,
  Entity,
  ManyToOne,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { Asset } from './Asset';
import { User } from './User';

@Entity()
export class ShoppingCentre {
  @PrimaryGeneratedColumn() public id: number;

  @Column() public name: string;

  @Column() public address: string;

  @Column() public createdBy: number;

  @Column() public updatedBy: number;

  @CreateDateColumn() public createdAt: string;

  @UpdateDateColumn() public updatedAt: string;

  // Relationships

  @OneToMany(() => Asset, (asset: Asset) => asset.shoppingCentre)
  public assets: Asset[];

  @ManyToOne(() => User, (user: User) => user.updatedAssets)
  public updatedUser: User;

  @ManyToOne(() => User, (user: User) => user.createdAssets)
  public createdUser: User;
}
