import {
  Column,
  CreateDateColumn,
  Entity,
  OneToMany,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';

import { Asset } from './Asset';
import { ShoppingCentre } from './ShoppingCentre';

@Entity()
export class User {
  @PrimaryGeneratedColumn() public id: number;

  @Column({ unique: true })
  public email: string;

  @Column() public firstName: string;

  @Column() public lastName: string;

  @Column() public age: number;

  @Column() public createdBy: number;

  @Column() public updatedBy: number;

  @CreateDateColumn() public createdAt: string;

  @UpdateDateColumn() public updatedAt: string;

  @OneToMany(() => Asset, (asset: Asset) => asset.updatedBy)
  public updatedAssets: Asset[];

  @OneToMany(() => Asset, (asset: Asset) => asset.createdBy)
  public createdAssets: Asset[];

  @OneToMany(
    () => ShoppingCentre,
    (shoppingCentre: ShoppingCentre) => shoppingCentre.updatedBy
  )
  public updatedShoppingCentres: ShoppingCentre[];

  @OneToMany(
    () => ShoppingCentre,
    (shoppingCentre: ShoppingCentre) => shoppingCentre.createdBy
  )
  public createdShoppingCentres: ShoppingCentre[];
}
