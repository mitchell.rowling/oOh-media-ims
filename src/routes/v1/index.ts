import * as Router from 'koa-router';

import { createAssetRouter } from '../v1/asset';
import { createLoginRouter } from '../v1/login';
import { createShoppingCentreRouter } from '../v1/shopping-centre';
import { createUserRouter } from '../v1/user';

const attachChildRouter = (
  parentRouter: Router,
  path: string,
  childRouter: Router
) => {
  parentRouter.use(path, childRouter.routes(), childRouter.allowedMethods());
};

export const attachPrivateRoutes = (router: Router) => {
  attachChildRouter(router, '/users', createUserRouter());
  attachChildRouter(router, '/assets', createAssetRouter());
  attachChildRouter(router, '/shopping-centres', createShoppingCentreRouter());
  return router;
};

export const createPrivateRouter = (options: Router.IRouterOptions = {}) =>
  attachPrivateRoutes(new Router(options));

export const attachPublicRoutes = (router: Router) => {
  attachChildRouter(router, '/login', createLoginRouter());
  return router;
};

export const createPublicRouter = (options: Router.IRouterOptions = {}) =>
  attachPublicRoutes(new Router(options));
