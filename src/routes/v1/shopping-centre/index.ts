import * as Router from 'koa-router';
import {
  shoppingCentreAssets,
  shoppingCentreCreate,
  shoppingCentreDeleteById,
  shoppingCentreFindById,
  shoppingCentreGetAll,
  shoppingCentrePatchById,
} from '../../../controllers/shopping-centre';

export const attachShoppingCentreRoutes = (router: Router) => {
  router.get('/', shoppingCentreGetAll);
  router.post('/', shoppingCentreCreate);
  router.get('/:id', shoppingCentreFindById);
  router.patch('/:id', shoppingCentrePatchById);
  router.delete('/:id', shoppingCentreDeleteById);
  router.get('/:id/assets', shoppingCentreAssets);
  return router;
};

export const createShoppingCentreRouter = (
  options: Router.IRouterOptions = {}
) => attachShoppingCentreRoutes(new Router(options));
