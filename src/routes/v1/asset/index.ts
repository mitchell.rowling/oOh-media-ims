import * as Router from 'koa-router';
import {
  assetCreate,
  assetDeleteById,
  assetFindById,
  assetGetAll,
  assetPatchById,
  assetSearchByActive,
  assetSearchByShoppingCentreName,
  assetUpdateStatus,
} from '../../..//controllers/asset';

export const attachAssetRoutes = (router: Router) => {
  router.get('/', assetGetAll);
  router.post('/', assetCreate);
  router.get('/:id', assetFindById);
  router.patch('/:id', assetPatchById);
  router.delete('/:id', assetDeleteById);
  router.post('/:id/activate', assetUpdateStatus(true));
  router.post('/:id/deactivate', assetUpdateStatus(false));
  router.get('/search/active', assetSearchByActive(true));
  router.get('/search/inactive', assetSearchByActive(false));
  router.get('/search/shopping-centre', assetSearchByShoppingCentreName);
  return router;
};

export const createAssetRouter = (options: Router.IRouterOptions = {}) =>
  attachAssetRoutes(new Router(options));
