import * as Router from 'koa-router';
import {
  userCreate,
  userDeleteById,
  userFindById,
  userGetAll,
  userPatchById,
} from '../../../controllers/user';

export const attachUserRoutes = (router: Router) => {
  router.get('/', userGetAll);
  router.post('/', userCreate);
  router.get('/:id', userFindById);
  router.patch('/:id', userPatchById);
  router.delete('/:id', userDeleteById);
  return router;
};

export const createUserRouter = (options: Router.IRouterOptions = {}) =>
  attachUserRoutes(new Router(options));
