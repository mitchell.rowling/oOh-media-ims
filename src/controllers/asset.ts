import { Context } from 'koa';
import { getManager } from 'typeorm';

import { Asset } from '../entity/Asset';
import { ShoppingCentre } from '../entity/ShoppingCentre';
import { create, deleteById, getAll, patchById, readById } from './core';

import { logger } from '../services/logger';

export const assetGetAll = getAll(Asset);
export const assetCreate = create(Asset);
export const assetFindById = readById(Asset);
export const assetPatchById = patchById(Asset);
export const assetDeleteById = deleteById(Asset);

export const assetUpdateStatus = (isActive: boolean) => async (
  context: Context
) => {
  const {
    user: { id },
  } = context.state;
  const assetRepository = getManager().getRepository(Asset);

  const updatedEntity = {
    updatedBy: id,
    isActive,
  };

  await assetRepository.update(context.params.id, updatedEntity);

  context.status = 204;
};

export const assetSearchByActive = (isActive: boolean) => async (
  context: Context
) => {
  const assetRepository = getManager().getRepository(Asset);
  const assets = await assetRepository.find({ isActive });
  if (!assets) {
    context.status = 404;
    return;
  }
  context.body = assets;
};

export const assetSearchByShoppingCentreName = async (context: Context) => {
  // get an entity repository to perform operations with entity
  const shoppingCentreRepository = getManager().getRepository(ShoppingCentre);

  logger.info(context.query.name);

  // load an entity by a given entity id
  const shoppingCentre = await shoppingCentreRepository.findOne({
    where: {
      name: context.query.name,
    },
    relations: ['assets'],
  });

  // if entity was not found return 404 to the client
  if (!shoppingCentre) {
    context.body = [];
    return;
  }
  // return loaded entity
  context.body = shoppingCentre.assets;
};
