import { User } from '../entity/User';

import { create, deleteById, getAll, patchById, readById } from './core';

export const userGetAll = getAll(User);
export const userCreate = create(User);
export const userFindById = readById(User);
export const userPatchById = patchById(User);
export const userDeleteById = deleteById(User);
