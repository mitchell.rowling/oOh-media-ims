import { Context } from 'koa';
import { getManager } from 'typeorm';

import { ShoppingCentre } from '../entity/ShoppingCentre';

import { create, deleteById, getAll, patchById, readById } from './core';

export const shoppingCentreGetAll = getAll(ShoppingCentre);
export const shoppingCentreCreate = create(ShoppingCentre);
export const shoppingCentreFindById = readById(ShoppingCentre);
export const shoppingCentrePatchById = patchById(ShoppingCentre);
export const shoppingCentreDeleteById = deleteById(ShoppingCentre);

export const shoppingCentreAssets = async (context: Context) => {
  // get an entity repository to perform operations with entity
  const shoppingCentreRepository = getManager().getRepository(ShoppingCentre);

  // load an entity by a given entity id
  const shoppingCentre = await shoppingCentreRepository.findOne({
    where: {
      id: context.params.id,
    },
    relations: ['assets'],
  });

  // if entity was not found return 404 to the client
  if (!shoppingCentre) {
    context.body = [];
    return;
  }
  // return loaded entity
  context.body = shoppingCentre.assets;
};
