openapi: "3.0.0"
info:
  version: 1.0.0
  title: oOh Media Inventory Management System
  description: "oOh! Media is designing a shopping centre inventory management system which will help their product management team maintain records of where physical display panels are installed in shopping centres. You are creating an API to manage inventory and shopping centres, allowing persisting and modifying data, as well as (optionally) a interface for users to manage the inventory."
  contact:
    name: "Mitchell Rowling"
    email: "mitchell.rowling@gmail.com"
servers:
  - url: http://localhost:8080/v1
paths:
  /login:
    post:
      summary: Login by providing email address
      operationId: findByEmail
      tags:
        - Login
      requestBody:
        description: Email is the only required key
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Login'
      responses:
        '200':
          description: A JWT
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/LoginResponse"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /users:
    get:
      summary: List all Users
      operationId: userGetAll
      tags:
        - Users
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Users"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    post:
      summary: Create a User
      operationId: userCreate
      tags:
        - Users
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserInput'
      responses:
        '201':
          description: Null response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /users/{UserId}:
    get:
      summary: Info for a specific User
      operationId: userFindById
      tags:
        - Users
      parameters:
        - name: UserId
          in: path
          required: true
          description: The id of the User to retrieve
          schema:
            type: string
      responses:
        '200':
          description: Expected response to a valid request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/User"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    patch:
      summary: Update Info for a specific User
      operationId: userPatchById
      tags:
        - Users
      parameters:
        - name: UserId
          in: path
          required: true
          description: The id of the User to update
          schema:
            type: string
      requestBody:
        description: Only updates the properties that are passed via the request body
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/UserInput'
      responses:
        '204':
          description: Null Response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    delete:
      summary: Delete a specific User
      operationId: userDeleteById
      tags:
        - Users
      parameters:
        - name: UserId
          in: path
          required: true
          description: The id of the User to delete
          schema:
            type: string
      responses:
        '204':
          description: Null Response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /assets:
    get:
      summary: List all Assets
      operationId: assetGetAll
      tags:
        - Assets
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Assets"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    post:
      summary: Create an Asset
      operationId: assetCreate
      tags:
        - Assets
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AssetInput'
      responses:
        '201':
          description: Null response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /assets/{AssetId}:
    get:
      summary: Info for a specific Asset
      operationId: assetFindById
      tags:
        - Assets
      parameters:
        - name: AssetId
          in: path
          required: true
          description: The id of the Asset to retrieve
          schema:
            type: string
      responses:
        '200':
          description: Expected response to a valid request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Asset"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    patch:
      summary: Update Info for a specific Asset
      operationId: assetPatchById
      tags:
        - Assets
      parameters:
        - name: AssetId
          in: path
          required: true
          description: The id of the Asset to update
          schema:
            type: string
      requestBody:
        description: Only updates the properties that are passed via the request body
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/AssetInput'
      responses:
        '204':
          description: Null Response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    delete:
      summary: Delete a specific Asset
      operationId: assetDeleteById
      tags:
        - Assets
      parameters:
        - name: AssetId
          in: path
          required: true
          description: The id of the Asset to delete
          schema:
            type: string
      responses:
        '204':
          description: Null Response
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Asset"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /assets/{AssetId}/activate:
    post:
      summary: Activate an Asset
      operationId: assetUpdateStatus(true)
      tags:
        - Assets
      parameters:
        - name: AssetId
          in: path
          required: true
          description: The id of the Asset to retrieve
          schema:
            type: string
      responses:
        '201':
          description: Null response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /assets/{AssetId}/deactivate:
    post:
      summary: Deactivate an Asset
      operationId: assetUpdateStatus(false)
      tags:
        - Assets
      parameters:
        - name: AssetId
          in: path
          required: true
          description: The id of the Asset to retrieve
          schema:
            type: string
      responses:
        '201':
          description: Null response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /assets/search/inactive:
    get:
      summary: List all inactive assets
      operationId: assetSearchByActive(false)
      tags:
        - Assets
      responses:
        '200':
          description: Expected response to a valid request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Assets"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /assets/search/active:
    get:
      summary: List all active assets
      operationId: assetSearchByActive(true)
      tags:
        - Assets
      responses:
        '200':
          description: Expected response to a valid request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Assets"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /assets/search/shopping-centre:
    get:
      summary: List all active assets
      operationId: assetSearchByShoppingCentreName
      tags:
        - Assets
      parameters:
        - name: name
          in: query
          description: Shopping Centre Name to search on
          schema:
            type: string
      responses:
        '200':
          description: Expected response to a valid request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Assets"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"

  /shopping-centres:
    get:
      summary: List all Shopping Centres
      operationId: shoppingCentreGetAll
      tags:
        - Shopping Centres
      responses:
        '200':
          description: ''
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ShoppingCentres"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    post:
      summary: Create a Shopping Centre
      operationId: shoppingCentreCreate
      tags:
        - Shopping Centres
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ShoppingCentreInput'
      responses:
        '201':
          description: Null response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /shopping-centres/{ShoppingCentreId}:
    get:
      summary: Info for a specific Shopping Centre
      operationId: shoppingCentreFindById
      tags:
        - Shopping Centres
      parameters:
        - name: ShoppingCentreId
          in: path
          required: true
          description: The id of the Shopping Centre to retrieve
          schema:
            type: string
      responses:
        '200':
          description: Expected response to a valid request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/ShoppingCentre"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    patch:
      summary: Update Info for a specific Shopping Centre
      operationId: shoppingCentrePatchById
      tags:
        - Shopping Centres
      parameters:
        - name: ShoppingCentreId
          in: path
          required: true
          description: The id of the Shopping Centre to update
          schema:
            type: string
      requestBody:
        description: Only updates the properties that are passed via the request body
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ShoppingCentreInput'
      responses:
        '204':
          description: Null Response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
    delete:
      summary: Delete a specific Shopping Centre
      operationId: shoppingCentreDeleteById
      tags:
        - Shopping Centres
      parameters:
        - name: ShoppingCentreId
          in: path
          required: true
          description: The id of the Shopping Centre to delete
          schema:
            type: string
      responses:
        '204':
          description: Null Response
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"
  /shopping-centres/{ShoppingCentreId}/assets:
    get:
      summary: Array of assets specific Shopping Centre
      operationId: shoppingCentreAssets
      tags:
        - Shopping Centres
      parameters:
        - name: ShoppingCentreId
          in: path
          required: true
          description: The id of the Shopping Centre to retrieve assets for
          schema:
            type: string
      responses:
        '200':
          description: Expected response to a valid request
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Assets"
        default:
          description: unexpected error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/Error"

security:
  - JWT: []
components:
  securitySchemes:
    JWT:            # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    User:
      required:
        - email
        - firstName
        - lastName
        - age
      properties:
        id:
          type: integer
        email:
          type: string
          format: email
        firstName:
          type: string
        lastName:
          type: string
        age:
          type: integer
        createdBy:
          type: integer
        updatedBy:
          type: integer
        updatedAt:
            type: string
            format: dateTime
        createdAt:
          type: string
          format: dateTime
    UserInput:
      required:
        - email
        - firstName
        - lastName
        - age
      properties:
        email:
          type: string
          format: email
        firstName:
          type: string
        lastName:
          type: string
        age:
          type: integer
    Users:
      type: array
      items:
        $ref: "#/components/schemas/User"
    Asset:
      required:
        - name
        - location
        - isActive
        - width
        - height
      properties:
        id:
          type: integer
        name:
          type: string
        location:
          type: string
        isActive:
          type: boolean
        width:
          type: integer
        height:
          type: integer
        shoppingCentreId:
          type: integer
        createdBy:
          type: integer
        updatedBy:
          type: integer
        updatedAt:
            type: string
            format: dateTime
        createdAt:
          type: string
          format: dateTime
    AssetInput:
      required:
        - name
        - location
        - isActive
        - width
        - height
      properties:
        name:
          type: string
        location:
          type: string
        isActive:
          type: boolean
        width:
          type: integer
        height:
          type: integer
        shoppingCentreId:
          type: integer
    Assets:
      type: array
      items:
        $ref: "#/components/schemas/Asset"
    ShoppingCentre:
      required:
        - name
        - address
      properties:
        id:
          type: integer
        name:
          type: string
        addresss:
          type: string
        createdBy:
          type: integer
        updatedBy:
          type: integer
        updatedAt:
            type: string
            format: dateTime
        createdAt:
          type: string
          format: dateTime
    ShoppingCentreInput:
      required:
        - name
        - address
      properties:
        name:
          type: string
        addresss:
          type: string
    ShoppingCentres:
      type: array
      items:
        $ref: "#/components/schemas/ShoppingCentre"
    Error:
      required:
        - code
        - message
      properties:
        code:
          type: integer
          format: int32
        message:
          type: string
    Login:
      required:
        - email
      properties:
        email:
          type: string
          format: email
    LoginResponse:
      required:
        - token
      properties:
        token:
          type: string
