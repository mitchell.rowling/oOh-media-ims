--
-- PostgreSQL database dump
--

-- Dumped from database version 10.1
-- Dumped by pg_dump version 10.3

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: asset; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.asset (
    id integer NOT NULL,
    name character varying NOT NULL,
    location character varying NOT NULL,
    "isActive" boolean NOT NULL,
    width integer NOT NULL,
    height integer NOT NULL,
    "createdBy" integer NOT NULL,
    "updatedBy" integer NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    "shoppingCentreId" integer,
    "updatedUserId" integer,
    "createdUserId" integer
);


ALTER TABLE public.asset OWNER TO postgres;

--
-- Name: asset_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.asset_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.asset_id_seq OWNER TO postgres;

--
-- Name: asset_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.asset_id_seq OWNED BY public.asset.id;


--
-- Name: shopping_centre; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.shopping_centre (
    id integer NOT NULL,
    name character varying NOT NULL,
    address character varying NOT NULL,
    "createdBy" integer NOT NULL,
    "updatedBy" integer NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedUserId" integer,
    "createdUserId" integer
);


ALTER TABLE public.shopping_centre OWNER TO postgres;

--
-- Name: shopping_centre_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.shopping_centre_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.shopping_centre_id_seq OWNER TO postgres;

--
-- Name: shopping_centre_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.shopping_centre_id_seq OWNED BY public.shopping_centre.id;


--
-- Name: user; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public."user" (
    id integer NOT NULL,
    email character varying NOT NULL,
    "firstName" character varying NOT NULL,
    "lastName" character varying NOT NULL,
    age integer NOT NULL,
    "createdBy" integer NOT NULL,
    "updatedBy" integer NOT NULL,
    "createdAt" timestamp without time zone DEFAULT now() NOT NULL,
    "updatedAt" timestamp without time zone DEFAULT now() NOT NULL
);


ALTER TABLE public."user" OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.user_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.user_id_seq OWNER TO postgres;

--
-- Name: user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.user_id_seq OWNED BY public."user".id;


--
-- Name: asset id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asset ALTER COLUMN id SET DEFAULT nextval('public.asset_id_seq'::regclass);


--
-- Name: shopping_centre id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shopping_centre ALTER COLUMN id SET DEFAULT nextval('public.shopping_centre_id_seq'::regclass);


--
-- Name: user id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user" ALTER COLUMN id SET DEFAULT nextval('public.user_id_seq'::regclass);


--
-- Name: asset PK_1209d107fe21482beaea51b745e; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asset
    ADD CONSTRAINT "PK_1209d107fe21482beaea51b745e" PRIMARY KEY (id);


--
-- Name: shopping_centre PK_2562a60f20b71a0310c7323395a; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shopping_centre
    ADD CONSTRAINT "PK_2562a60f20b71a0310c7323395a" PRIMARY KEY (id);


--
-- Name: user PK_cace4a159ff9f2512dd42373760; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "PK_cace4a159ff9f2512dd42373760" PRIMARY KEY (id);


--
-- Name: user UQ_e12875dfb3b1d92d7d7c5377e22; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public."user"
    ADD CONSTRAINT "UQ_e12875dfb3b1d92d7d7c5377e22" UNIQUE (email);


--
-- Name: shopping_centre FK_4a66c7c1bfa80e3a2bae0a8a495; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shopping_centre
    ADD CONSTRAINT "FK_4a66c7c1bfa80e3a2bae0a8a495" FOREIGN KEY ("updatedUserId") REFERENCES public."user"(id);


--
-- Name: shopping_centre FK_4a9bd06ed0959daa4e2c4c158dc; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.shopping_centre
    ADD CONSTRAINT "FK_4a9bd06ed0959daa4e2c4c158dc" FOREIGN KEY ("createdUserId") REFERENCES public."user"(id);


--
-- Name: asset FK_c374c2d083ba8fd04f13131da41; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asset
    ADD CONSTRAINT "FK_c374c2d083ba8fd04f13131da41" FOREIGN KEY ("createdUserId") REFERENCES public."user"(id);


--
-- Name: asset FK_ce11fbb50fc9fe318674e06714b; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asset
    ADD CONSTRAINT "FK_ce11fbb50fc9fe318674e06714b" FOREIGN KEY ("updatedUserId") REFERENCES public."user"(id);


--
-- Name: asset FK_f61c22fadaed8063c9d9a43d666; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.asset
    ADD CONSTRAINT "FK_f61c22fadaed8063c9d9a43d666" FOREIGN KEY ("shoppingCentreId") REFERENCES public.shopping_centre(id);


--
-- Name: SCHEMA public; Type: ACL; Schema: -; Owner: postgres
--

GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--



INSERT INTO public."user"(
	email, "firstName", "lastName", age, "createdBy", "updatedBy","createdAt", "updatedAt")
VALUES ('mitchell.rowling@gmail.com',	'Mitchell','Rowling',	26,	0, 0, '2018-07-31 17:33:37.681312', '2018-07-31 17:33:37.681312');
