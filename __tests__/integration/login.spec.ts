import * as supertest from 'supertest';

const request = supertest('http://api:8080');

describe('/v1/login', async () => {
  test('should return a token when succesfully logs in', async () => {
    const response = await request.post('/v1/login').send({
      email: 'mitchell.rowling@gmail.com',
    });
    expect(response.status).toBe(200);
    expect(response.body.token).toBeTruthy();
  });
  describe('When Unsuccesfully logs in', () => {
    test('should return Unauthorized', async () => {
      const response = await request.post('/v1/login').send({
        email: 'invalidemail',
      });
      expect(response.text).toBe('Unauthorized');
    });
    test('should return status 401', async () => {
      const response = await request.post('/v1/login').send({
        email: 'invalidemail',
      });
      expect(response.status).toBe(401);
    });
  });
});
